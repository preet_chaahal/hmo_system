from django.db import models


class Zone(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    zone_zip = models.IntegerField(10)
    country_zone = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    states = models.CharField(max_length=50)
    ss = models.CharField(max_length=50)
    zone = models.ForeignKey(Zone)
    regions = models.CharField(max_length=50)
    ls = models.CharField(max_length=50)


class Employee(models.Model):
    us = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    user_login = models.CharField(max_length=50)
    user_type_group = models.ForeignKey(Zone)
    phone = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    sex = models.CharField(max_length=50)
    address_id = models.CharField(max_length=50)
    bvn = models.ForeignKey(Zone)
    ps = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)
    transaction_emp = models.CharField(max_length=50)


class State(models.Model):
    country = models.ForeignKey(Country)
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    regions = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    ls = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    state = models.ForeignKey(State)
    ss = models.CharField(max_length=50)
    cs = models.CharField(max_length=50)
    ls = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class LGA(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    regions = models.ForeignKey(Region)
    state = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Address(models.Model):
    us = models.CharField(max_length=50)
    address_id = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    zip_code = models.CharField(max_length=10)
    state = models.ForeignKey(State)
    user_type_group = models.CharField(max_length=50)
    emp_address = models.CharField(max_length=50)
    agent = models.CharField(max_length=50)
    ds = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)


class Symptom(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class MedicalProcedure(models.Model):
    us = models.CharField(max_length=50)
    name = models.TextField()
    code = models.CharField(max_length=50)
    diseases = models.ForeignKey(State)
    drugs = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)


    def __str__(self):
        return self.name[0:50]


class Drug(models.Model):
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=50)
    diseases = models.ForeignKey(State)
    us = models.CharField(max_length=50)
    ms = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Disease(models.Model):
    drugs = models.ManyToManyField(Drug)
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    symptoms = models.ManyToManyField(Symptom)
    ms = models.ManyToManyField(MedicalProcedure)

    def __str__(self):
        return self.name


class Service(models.Model):
    us = models.CharField(max_length=50)
    service_type = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class ProductPlan(models.Model):
    name = models.CharField(max_length=50)
    ps = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=30, decimal_places=4)
    product_plan = models.CharField(max_length=50)
    payments = models.CharField(max_length=50)
    subscription = models.CharField(max_length=50)
    validity = models.CharField(max_length=50)
    age_group = models.CharField(max_length=30)  # We should actually create choices for this.
    valid_till = models.DateTimeField()
    valid_from = models.DateTimeField()
    transaction = models.CharField(max_length=50)
