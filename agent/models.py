from django.db import models

# Create your models here.
class Agent(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    sex = models.IntegerField(10)
    email = models.CharField(max_length=50)
    age = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    us = models.IntegerField(10)
    cs = models.CharField(max_length=50)
    agent_type = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class AgentType(models.Model):
    agent = models.CharField(max_length=50)


class AgentRequestType(models.Model):
    request = models.CharField(max_length=50)
    done = models.CharField(max_length=50)
    pending = models.CharField(max_length=50)
    cancel = models.CharField(max_length=50)
    approval_request = models.CharField(max_length=50)


class Commission(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    commission_type = models.CharField(max_length=50)
    agent = models.CharField(max_length=50)


class Service(models.Model):
    us = models.CharField(max_length=50)
    service_type = models.CharField(max_length=50)
    transaction = models.CharField(max_length=50)


class ServiceType(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    ss = models.CharField(max_length=50)


class Subscription(models.Model):
    users = models.CharField(max_length=50)
    ps = models.CharField(max_length=50)
    us = models.IntegerField(10)
    product_plan = models.CharField(max_length=50)
    user_subs = models.CharField(max_length=50)
    user_hmo = models.CharField(max_length=50)